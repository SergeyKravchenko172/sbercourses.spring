package com.sber.java15.spring.SpringProjectLibraryFilm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProjectLibraryFilmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectLibraryFilmApplication.class, args);
	}

}
